Select * from shopping;

CREATE TABLE shopping (id INTEGER PRIMARY KEY, name TEXT, amount INTEGER);
DROP TABLE shopping;

ALTER table shopping RENAME to shopp;
ALTER table shopp RENAME to shopping;

INSERT INTO shopping (id , name , amount ) VALUES (1, 'Avokado', 5);
INSERT INTO shopping VALUES (2, 'Milk', 2);
INSERT INTO shopping VALUES (3, 'Bread', 3);
INSERT INTO shopping VALUES (4, 'Chocolate', 8);
INSERT INTO shopping VALUES (5, 'Bamba', 5);
INSERT INTO shopping VALUES (6, 'Orange', 10);

select id, name from shopping;

SELECT * FROM shopping WHERE amount > 5;
SELECT * FROM shopping WHERE amount = 2;
SELECT * FROM shopping WHERE name LIKE 'B%';

DELETE FROM shopping
where amount = 5 or name like 'B%';

UPDATE shopping
SET name = 'Bisli'
where name = 'Bamba';

update shopping
set amount = amount + 1, name = 'MILK'
where name = 'MILK';

ALTER TABLE shopping ADD COLUMN maavar;

-- ALTER TABLE shopping DROP COULMN maavar; -- sqlite not support alter column, also no drop

UPDATE shopping SET maavar=3 WHERE id=1;
UPDATE shopping SET maavar=12 WHERE id=2;
UPDATE shopping SET maavar=12 WHERE id=3;
UPDATE shopping SET maavar=5 WHERE id=4;
UPDATE shopping SET maavar=5 WHERE id=5;
UPDATE shopping SET maavar=5 WHERE id=6;

SELECT * FROM shopping WHERE amount > 1 AND maavar > 5;
SELECT * FROM shopping WHERE maavar BETWEEN 3 AND 5;

update shopping
set amount = 20
where id = 3;

select * from shopping
order by amount desc, maavar desc;

CREATE TABLE books (id INTEGER PRIMARY KEY, name TEXT);
INSERT INTO books VALUES (1, 'SQL PROGRAMMING');
INSERT INTO books VALUES (2, 'CSHARP PROGRAMMING');
DELETE FROM books;
select * from books;

select COUNT(*) TOTAL_COUNT, MAX(amount) MAX_AMOUNT, AVG(amount) AVG_AMOUNT , MIN(amount) MIN_AMOUNT from shopping;
SELECT MAX(amount) MAX_AMOUNT from shopping;
SELECT AVG(amount) AVG_AMOUNT from shopping;
SELECT MIN(amount) MIN_AMOUNT from shopping;

INSERT INTO shopping VALUES (7, 'Onions', 3, 6);
INSERT INTO shopping VALUES (8, 'Orio', 1, 8);

SELECT * FROM shopping
ORDER BY maavar, AMOUNT DESC

SELECT MAAVAR , COUNT(*) as items_per_maavar, MAX(AMOUNT) FROM shopping
where amount > 0
GROUP BY MAAVAR
having items_per_maavar > 1; -- group filter condition

update shopping
set amount = 0
where id=5;

CREATE TABLE prices (id INTEGER PRIMARY KEY, price real);
INSERT INTO prices VALUES (1, 3.5);
INSERT INTO prices VALUES (2, 7.21);
INSERT INTO prices VALUES (3, 12.89);
INSERT INTO prices VALUES (4, 5.5);
INSERT INTO prices VALUES (5, 3.31);
INSERT INTO prices VALUES (6, 2.0);
INSERT INTO prices VALUES (7, 10.4);
INSERT INTO prices VALUES (12, 10.4);

select * from prices

Select * from shopping;

SELECT  s.id,s.name,s.amount,s.maavar, p.price FROM shopping s
    join prices p
    on s.id = p.id;

-- workaround right join (does not exist in sqlite)
SELECT  shopping.id,shopping.name,shopping.amount,shopping.maavar, prices.price FROM shopping
    left join prices
    on shopping.id = prices.id;

select maavar, max(price) from
(SELECT  shopping.id,shopping.name,shopping.amount,shopping.maavar, prices.price FROM shopping
    join prices
    on shopping.id = prices.id
    where amount > 0)
group by maavar
having max(price) > 10

INSERT INTO shopping VALUES (9, 'Beer', 30, 5);
UPDATE prices
SET price=50.01
WHERE id=12

SELECT  s.id,s.name,s.amount,s.maavar, p.price FROM shopping s
    join prices p
    on s.id = p.id
    where price > 5;

-- *etgar: how to find the most expensive product after join?
SELECT  s.id,s.name,s.amount,s.maavar, p.price FROM shopping s
    join prices p
    on s.id = p.id
    where p.price = (select max(price) from prices);

select * from prices

* now you ready for targil 2







